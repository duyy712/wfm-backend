namespace WFM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditModel4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerRequest", "Employee_Code", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code1", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code2", "dbo.Employee");
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code1" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code2" });
            DropColumn("dbo.CustomerRequest", "Employee_Code");
            DropColumn("dbo.CustomerRequest", "Employee_Code1");
            DropColumn("dbo.CustomerRequest", "Employee_Code2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomerRequest", "Employee_Code2", c => c.String(maxLength: 128));
            AddColumn("dbo.CustomerRequest", "Employee_Code1", c => c.String(maxLength: 128));
            AddColumn("dbo.CustomerRequest", "Employee_Code", c => c.String(maxLength: 128));
            CreateIndex("dbo.CustomerRequest", "Employee_Code2");
            CreateIndex("dbo.CustomerRequest", "Employee_Code1");
            CreateIndex("dbo.CustomerRequest", "Employee_Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code2", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code1", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code", "dbo.Employee", "Code");
        }
    }
}
