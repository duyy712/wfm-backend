namespace WFM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditModel2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerRequest", "Employee_Code", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code1", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code2", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code1_Code", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code2_Code", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "Employee_Code3_Code", "dbo.Employee");
            DropForeignKey("dbo.CustomerRequest", "DescriptionFileId", "dbo.SourceFile");
            DropForeignKey("dbo.CustomerRequest", "TopicId", "dbo.Topic");
            DropIndex("dbo.CustomerRequest", new[] { "TopicId" });
            DropIndex("dbo.CustomerRequest", new[] { "DescriptionFileId" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code1" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code2" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code1_Code" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code2_Code" });
            DropIndex("dbo.CustomerRequest", new[] { "Employee_Code3_Code" });
            DropTable("dbo.CustomerRequest");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CustomerRequest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        FromEmployeeCode = c.String(),
                        ToEmployeeCode = c.String(),
                        DoEmployeeCode = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DesignDateExpired = c.DateTime(),
                        CodeDateExpired = c.DateTime(),
                        Description = c.String(),
                        DevDescription = c.String(),
                        DescriptionFileId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        Employee_Code = c.String(maxLength: 128),
                        Employee_Code1 = c.String(maxLength: 128),
                        Employee_Code2 = c.String(maxLength: 128),
                        Employee_Code1_Code = c.String(maxLength: 128),
                        Employee_Code2_Code = c.String(maxLength: 128),
                        Employee_Code3_Code = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.CustomerRequest", "Employee_Code3_Code");
            CreateIndex("dbo.CustomerRequest", "Employee_Code2_Code");
            CreateIndex("dbo.CustomerRequest", "Employee_Code1_Code");
            CreateIndex("dbo.CustomerRequest", "Employee_Code2");
            CreateIndex("dbo.CustomerRequest", "Employee_Code1");
            CreateIndex("dbo.CustomerRequest", "Employee_Code");
            CreateIndex("dbo.CustomerRequest", "DescriptionFileId");
            CreateIndex("dbo.CustomerRequest", "TopicId");
            AddForeignKey("dbo.CustomerRequest", "TopicId", "dbo.Topic", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CustomerRequest", "DescriptionFileId", "dbo.SourceFile", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CustomerRequest", "Employee_Code3_Code", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code2_Code", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code1_Code", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code2", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code1", "dbo.Employee", "Code");
            AddForeignKey("dbo.CustomerRequest", "Employee_Code", "dbo.Employee", "Code");
        }
    }
}
