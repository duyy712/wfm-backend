namespace WFM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditModel1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CustomerRequest", "Employee_Code1");
            DropIndex("dbo.CustomerRequest", "Employee_Code1");
        }
        
        public override void Down()
        {
        }
    }
}
