namespace WFM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditModel3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerRequest",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    TopicId = c.Int(nullable: false),
                    FromEmployeeCode = c.String(),
                    ToEmployeeCode = c.String(),
                    DoEmployeeCode = c.String(),
                    DateCreated = c.DateTime(nullable: false),
                    DesignDateExpired = c.DateTime(),
                    CodeDateExpired = c.DateTime(),
                    Description = c.String(),
                    DevDescription = c.String(),
                    DescriptionFileId = c.Int(),
                    State = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SourceFile", t => t.DescriptionFileId, cascadeDelete: true)
                .ForeignKey("dbo.Topic", t => t.TopicId, cascadeDelete: true)

                .Index(t => t.TopicId)
                .Index(t => t.DescriptionFileId);
               
            
        }
        
        public override void Down()
        {
  
            DropForeignKey("dbo.CustomerRequest", "TopicId", "dbo.Topic");
            DropForeignKey("dbo.CustomerRequest", "DescriptionFileId", "dbo.SourceFile");
          
            DropIndex("dbo.CustomerRequest", new[] { "DescriptionFileId" });
            DropIndex("dbo.CustomerRequest", new[] { "TopicId" });
            DropTable("dbo.CustomerRequest");
        }
    }
}
