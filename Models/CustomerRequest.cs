﻿namespace WFM.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerRequest")]
    public partial class CustomerRequest
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TopicId { get; set; }

        public string FromEmployeeCode { get; set; }

        public string ToEmployeeCode { get; set; }

        public string DoEmployeeCode { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DesignDateExpired { get; set; }

        public DateTime? CodeDateExpired { get; set; }

        public string Description { get; set; }

        public string DevDescription { get; set; }

        public int DescriptionFileId { get; set; }

        public int State { get; set; }

        public virtual SourceFile SourceFile { get; set; }

        public virtual Topic Topic { get; set; }
    }
}
