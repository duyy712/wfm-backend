namespace WFM.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee    {
       
        [Key]
       
        public string Code { get; set; }

        public string Name { get; set; }    

        public string Email { get; set; }  

    }
}
